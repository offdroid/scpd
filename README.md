# Scpd

Collection of experiment config files.
Not all files can be loaded by all sorl/bcq versions.

Example script to generate job file. Prefix passed as first and only parameter
```sh
#!/bin/sh
find -name "$1*" -printf '%f\n' | sed 's/^/bcq --num-threads 1 --hide-bar --print-summary --path \/app\/presets\//' > "job=$1.txt"
```